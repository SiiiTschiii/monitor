package monitor

import (
	"fmt"
	"github.com/mackerelio/go-osstat/cpu"
	"github.com/mackerelio/go-osstat/disk"
	"github.com/mackerelio/go-osstat/memory"
	"github.com/mackerelio/go-osstat/network"
	log "github.com/sirupsen/logrus"
	"math"
	"net/http"
	"path/filepath"
	"runtime"
	"time"
)

// Timed is a middleware that measures and logs the time it takes for the function to execute
func Timed(f func(http.ResponseWriter, *http.Request)) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		f(w, r)
		end := time.Now()
		duration := end.Sub(start)
		log.WithFields(log.Fields{"request": r.URL.Path, "duration": duration.Nanoseconds() / 1e6}).Info("The request took ", duration)
	}
}

// Elapsed can measure the exection time of the calling function.
// `defer Elapsed()()` needs to be called at the beginning of a function body.
// Logs the fn name and execution time
func Elapsed() func() {
	start := time.Now()
	return func() {
		duration := time.Since(start)
		_, _, fnName := CallerRef(1)
		log.WithFields(log.Fields{"function": fnName, "duration": duration.Nanoseconds() / 1e6}).Info(fmt.Sprintf("%s took %v", fnName, duration))

	}
}

// CallerRef returns the filename, code line and function name of the stack context specified by skip.
// skip = 0 -> context of the calling function.
func CallerRef(skip int) (file string, line int, fnName string) {
	pc, file, line, ok := runtime.Caller(skip + 1)
	if !ok {
		file = "?"
		line = 0
	}

	fn := runtime.FuncForPC(pc)
	if fn == nil {
		fnName = "?()"
	} else {
		fnName = filepath.Base(fn.Name())
	}

	return filepath.Base(file), line, fnName
}

func findDisk(stats []disk.Stats, diskName string) (stat disk.Stats, ok bool) {
	for _, stat := range stats {
		if stat.Name == diskName {
			return stat, true
		}
	}
	return disk.Stats{}, false
}

func findNetworkIF(stats []network.Stats, netIFName string) (stat network.Stats, ok bool) {
	for _, stat := range stats {
		if stat.Name == netIFName {
			return stat, true
		}
	}
	return network.Stats{}, false
}

// LogResourceConsumption logs the current memory, cpu, disk iops and network rx/tx (Bytes) usage.
// Does one measurement before and after duration
// Blocks for the given duration.
func LogResourceConsumption(duration time.Duration, netIFName string, diskName string) {

	// measurement 1
	cpuBefore, err := cpu.Get()
	networkBefore, err := network.Get()
	diskBefore, err := disk.Get()
	memoryBefore, err := memory.Get()
	if err != nil {
		return
	}

	time.Sleep(duration)

	// measurement 2
	cpuAfter, err := cpu.Get()
	diskAfter, err := disk.Get()
	networkAfter, err := network.Get()
	memoryAfter, err := memory.Get()
	if err != nil {
		return
	}

	resources := log.Fields{
		"timestamp": time.Now().UnixNano() / 1e6,
	}

	// log cpu usage
	totalCPU := float64(cpuAfter.Total - cpuBefore.Total)
	resources["cpuIdle"] = int64(math.Round(float64(cpuAfter.Idle-cpuBefore.Idle) / totalCPU * 100))
	resources["cpuUser"] = int64(math.Round(float64(cpuAfter.User-cpuBefore.User) / totalCPU * 100))
	resources["cpuSystem"] = int64(math.Round(float64(cpuAfter.System-cpuBefore.System) / totalCPU * 100))
	resources["cpuNice"] = int64(math.Round(float64(cpuAfter.Nice-cpuBefore.Nice) / totalCPU * 100))
	resources["cpuIOWait"] = int64(math.Round(float64(cpuAfter.Iowait-cpuBefore.Iowait) / totalCPU * 100))
	resources["cpuIrq"] = int64(math.Round(float64(cpuAfter.Irq-cpuBefore.Irq) / totalCPU * 100))
	resources["cpuSoftIrq"] = int64(math.Round(float64(cpuAfter.Softirq-cpuBefore.Softirq) / totalCPU * 100))
	resources["cpuSteal"] = int64(math.Round(float64(cpuAfter.Steal-cpuBefore.Steal) / totalCPU * 100))

	// log average memory consumption
	totalMemory := float64(memoryAfter.Total+memoryBefore.Total) / 2
	resources["memoryTotal"] = int64(totalMemory)
	resources["memoryUsed"] = int64(math.Round(float64(memoryAfter.Used+memoryBefore.Used) / (totalMemory * 2) * 100))
	resources["memoryCached"] = int64(math.Round(float64(memoryBefore.Cached+memoryAfter.Cached) / (totalMemory * 2) * 100))
	resources["memoryFree"] = int64(math.Round(float64(memoryBefore.Free+memoryAfter.Free) / (totalMemory * 2) * 100))

	// log disk read/write IOPS when disk found
	if diskStatBefore, ok := findDisk(diskBefore, diskName); ok {

		if diskStatAfter, ok := findDisk(diskAfter, diskName); ok {
			resources["diskName"] = diskStatBefore.Name
			resources["diskReads"] = int64(float64(diskStatAfter.ReadsCompleted-diskStatBefore.ReadsCompleted) / duration.Seconds())
			resources["diskWrites"] = int64(float64(diskStatAfter.WritesCompleted-diskStatBefore.WritesCompleted) / duration.Seconds())
		}

	}

	//log network rx/tx in B/s when network interface found
	if networkStatBefore, ok := findNetworkIF(networkBefore, netIFName); ok {
		if networkStatAfter, ok := findNetworkIF(networkAfter, netIFName); ok {
			resources["ifName"] = networkStatBefore.Name
			resources["netRx"] = int64(float64(networkStatAfter.RxBytes-networkStatBefore.RxBytes) / duration.Seconds())
			resources["netTx"] = int64(float64(networkStatAfter.TxBytes-networkStatBefore.TxBytes) / duration.Seconds())
		}
	}

	log.WithFields(resources).Info(fmt.Sprintf("Current resource consumption"))
}
