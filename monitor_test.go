package monitor

import (
	"bytes"
	"context"
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"net/http"
	"strings"
	"testing"
	"time"
)

func startHTTPServer() *http.Server {
	srv := &http.Server{Addr: ":3000"}

	http.HandleFunc("/", Timed(hello))

	go func() {
		// returns ErrServerClosed on graceful close
		if err := srv.ListenAndServe(); err != http.ErrServerClosed {
			// NOTE: there is a chance that next line won't have time to run,
			// as main() doesn't wait for this goroutine to stop. don't use
			// code with race conditions like these for production. see post
			// comments below on more discussion on how to handle this.
			log.Fatalf("ListenAndServe(): %s", err)
		}
	}()

	// returning reference so caller can call Shutdown()
	return srv
}

func hello(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "<h1>Hello!</h1>")
}

func TestMeasureRequestTime(t *testing.T) {

	var logBuffer bytes.Buffer
	log.SetOutput(&logBuffer)

	srv := startHTTPServer()

	_, err := http.Get("http://localhost:3000")
	assert.Equal(t, nil, err)

	err = srv.Shutdown(context.Background())
	assert.Equal(t, nil, err)

	assert.Equal(t, true, strings.Contains(logBuffer.String(), "The request took"))
}

func TestMeasureResourceConsumption(t *testing.T) {
	LogResourceConsumption(time.Millisecond*5000, "wlp4s0", "nvme0n1")
}
