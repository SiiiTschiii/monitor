module gitlab.ethz.ch/chgerber/monitor

go 1.12

require (
	github.com/mackerelio/go-osstat v0.0.0-20190208070000-fcbbc2a9dcc2
	github.com/sirupsen/logrus v1.4.1
	github.com/stretchr/testify v1.3.0
	golang.org/x/crypto v0.0.0-20190513172903-22d7a77e9e5f // indirect
	golang.org/x/lint v0.0.0-20190409202823-959b441ac422 // indirect
	golang.org/x/net v0.0.0-20190522155817-f3200d17e092 // indirect
	golang.org/x/sys v0.0.0-20190527104216-9cd6430ef91e // indirect
	golang.org/x/text v0.3.2 // indirect
	golang.org/x/tools v0.0.0-20190525145741-7be61e1b0e51 // indirect
)
