# monitor
Monitoring and logging methods.

[![pipeline status](https://gitlab.ethz.ch/chgerber/monitor/badges/master/pipeline.svg)](https://gitlab.ethz.ch/chgerber/monitor/commits/master)
[![coverage report](https://gitlab.ethz.ch/chgerber/monitor/badges/master/coverage.svg)](https://gitlab.ethz.ch/chgerber/monitor/commits/master)